---
layout: page
title: Web Application
permalink: /web_app/
---

## Change Password

1. Open the password form [Change Password](https://www.mymemelist.com/user/manage)
![Change Password]()
2. Enter old and new password
3. Click *Change Password*


## Browse Memes

![Meme entry](/maas/documentation/images/meme_entry.png)
The title is clickable and opens the original link in a separate tab.
On the bottom, the domain, tags and category of the specific meme are shown as bubbles. 

The *Find Similar* button triggers the similarity function. It sorts your memes based on the similarity to the selected meme.

*Remove Meme* deletes the meme from your list directly. There is no second prompt.


## Filter

Start typing in one of the fields and use autocomplete to set a filter on a field, e.g. hugelol.com on hostname.
![Register form](/maas/documentation/images/filter_rules.png)


## Logout

To logout, click the logout button on the upper right corner.