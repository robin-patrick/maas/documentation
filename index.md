---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

This is the official documentation for [MyMemeList](https://www.mymemelist.com/).

---


## Getting started

Follow the steps below to create an account and login to MyMemeList. If need help to reset your password or browse your memes, refer to the section [Web App](/maas/documentation/web_app/).


### Register

1. Open [MyMemeList](https://www.mymemelist.com/) frontpage.
![Register form](/maas/documentation/images/register.png)
2. Click register
3. Enter username, email and password
![Register form](/maas/documentation/images/register_2.png)


### Confirm Registration

1. Confirmation form appears
![Register form](/maas/documentation/images/confirm.png)

2. Enter your username and the code you receive via email.

	![Register form](/maas/documentation/images/confirmation_mail.png)


### Login

1. Click on login
![Register form](/maas/documentation/images/login_after_register.png)

2. After successful login, you are redirected to your personal meme list.
![Register form](/maas/documentation/images/after_login.png)
The left blue button refreshs the meme list, the green on ein the middle opens the form for adding memes manually, and the right one opens a popup with several filters, e.g. for restricting the file type or domain of the memes shown below.

### Add your first Meme

1. You can use the [Browser Extensions](/maas/documentation/browser_extensions) or manually paste the link in the add form:

	![Register form](/maas/documentation/images/add_memes_web_app.png)

---

## Feedback

If you find bugs or want to give feedback, please open an issue here:
[MyMemeList Issues](https://gitlab.com/robin-patrick/maas/documentation/-/issues)

Also, Allions members can report issues in the discord channel *#mymemelist*.