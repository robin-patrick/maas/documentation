---
layout: page
title: Browser Extensions
permalink: /browser_extensions/
---
## Browser Extentsion

### Chrome

1. Add the extension to your chrome browser from [chrome web store](https://chrome.google.com/webstore/detail/my-meme-list/olideoepjjedlmmaodpmcnekeoonjcgg)
2. Click "add extension"
3. In the rupper right corner, click the addons symbol and select options from its submenu
![Image](/maas/documentation/images/chrome_menu_options.png)
5. Click "Extension Options" and enter your username and password, then save
![Image](/maas/documentation/images/chrome_options.png)
6. Navigate to a meme in one of your tabs (e.g https://hugelol.com/lol/690999)
7. Click the browser extensions icon in the upper right corner 
8. A popup appears and displays "Meme parsed and stored successfully."

### Firefox

1. Add the extension to your firefox browser from [Firefox store](https://addons.mozilla.org/de/firefox/addon/my-meme-list/)
2. Type "about:addons" in a new browser tab and hit enter
3. Select category extensions
4. In the list of all your extensions, click on MyMemeList
5. Select settings and enter your username and password, then save
6. Navigate to a meme in one of your tabs (e.g https://hugelol.com/lol/690999)
7. Click the browser extensions icon in the upper right corner 
8. A popup appears and displays "Meme parsed and stored successfully."